"use strict";

angular.module("foodzOnline.thankyou", ["ngRoute"])

.config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/thankyou", {
        templateUrl: "thankyou/thankyou.html",
        controller: "ThankYouController"
    });
}])

.controller("ThankYouController", function($scope, orderListService, userAddressService) {
    // Receive the data in the form of a service
    // And display that on the screen
    $scope.address = userAddressService.address;

    $scope.totalBill = orderListService.totalBill;
    // Order ID generate
    // Price of the order

    $scope.orderId = orderListService.generateOrderId();
});