"use strict";

angular.module("foodzOnline.checkout", ["ngRoute"])

.config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/checkout", {
        templateUrl: "checkout/checkout.html",
        controller: "CheckoutController"
    });
}])

.controller("CheckoutController", function($scope, $filter, $location, foodMenuService, orderListService, userAddressService) {
    $scope.isNavCollapsed = true;

    $scope.orderList = orderListService.orderList || [];
    console.log(orderListService.orderList);

    $scope.totalItems = orderListService.totalItems;
    $scope.totalBill = orderListService.totalBill;

    $scope.foodMenu = foodMenuService.foodMenu;

    $scope.addItem = function(cuisineType, foodItemIndex) {
        orderListService.addItem($scope, $filter, foodMenuService, cuisineType, foodItemIndex);
    }

    $scope.removeItem = function(cuisineType, foodItemIndex) {
        orderListService.removeItem($scope, $filter, foodMenuService, cuisineType, foodItemIndex);
    };

    $scope.confirmOrder = function(address) {
        userAddressService.address = {
            firstName: address.firstName,
            lastName: address.lastName,
            addressLine1: address.addressLine1,
            addressLine2: address.addressLine2,
            cityName: address.cityName,
            stateName: address.stateName,
            pincode: address.pincode,
            phoneNumber: address.phoneNumber
        };
        $location.path("/"+ "thankyou");
    };
});