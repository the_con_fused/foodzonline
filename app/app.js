'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('foodzOnline', [
    'ngRoute',
    'ui.bootstrap',
    'foodzOnline.home',
    'foodzOnline.checkout',
    'foodzOnline.thankyou'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
    
    $routeProvider.otherwise({redirectTo: '/home'});
}]);

app.service("foodMenuService", function() {
    this.foodMenu = [
        {
            id: 0,
            cuisine: "South Indian",
            foodItems: [
                {
                    id: 0,
                    name: "Idly",
                    price: 40,
                    stock: 100
                },
                {
                    id: 1,
                    name: "Vada",
                    price: 50,
                    stock: 0
                },
                {
                    id: 2,
                    name: "Plain Dosa",
                    price: 50,
                    stock: 10
                },
                {
                    id: 3,
                    name: "Masala Dosa",
                    price: 80,
                    stock: 1
                },
                {
                    id: 4,
                    name: "Paneer Dosa",
                    price: 90,
                    stock: 100
                },
                {
                    id: 5,
                    name: "Onion Uttapam",
                    price: 80,
                    stock: 100
                }
            ]
        },
        {
            id: 1,
            cuisine: "North Indian",
            foodItems: [
                {
                    id: 0,
                    name: "Samosa",
                    price: 20,
                    stock: 100
                },
                {
                    id: 1,
                    name: "Poori Aloo Dum",
                    price: 40,
                    stock: 100
                },
                {
                    id: 2,
                    name: "Aloo Paratha",
                    price: 50,
                    stock: 100
                },
                {
                    id: 3,
                    name: "Masala Paratha",
                    price: 80,
                    stock: 100
                },
                {
                    id: 4,
                    name: "Chana Kulcha",
                    price: 80,
                    stock: 100
                },
                {
                    id: 5,
                    name: "Choley Bhaturey",
                    price: 100,
                    stock: 100
                }
            ]
        }
    ];
});

app.service("orderListService", function() {
    this.orderList = null;
    this.orderId = 0;

    this.addItem = function($scope, $filter, foodMenuService, cuisineType, foodItemIndex) {
        console.log(cuisineType + ", " + foodItemIndex);

        var found = $filter("getFoodItemByIndex")($scope.orderList, foodItemIndex, cuisineType);

        if(found) {
            // If item is in stock
            // Then increment
            console.log(foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex].stock);

            if(foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex].stock > 0) {
                foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex].stock -= 1;
                found.item.quantity += 1;
            } else {
                alert("Item out of stock");
                return false;
            }

            // Else show that out of stock
        } else {
            // If item is in stock
            // Then push, else show some error

            console.log(foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex].stock);

            if(foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex].stock > 0) {
                foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex].stock -= 1;
            } else {
                alert("Item out of stock");
                return false;
            }

            $scope.orderList.push({
                quantity: 1,
                item: foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex],
                cuisineType: cuisineType,
                foodItemIndex: foodItemIndex,
            });
        }
        $scope.totalItems += 1;
        $scope.totalBill += foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex].price;

        console.log($scope.orderList);
    };

    this.removeItem = function($scope, $filter, foodMenuService, cuisineType, foodItemIndex) {
        console.log(cuisineType + ", " + foodItemIndex);

        var found = $filter("getFoodItemByIndex")($scope.orderList, foodItemIndex, cuisineType);
        console.log(found);

        if(found) {
            if(found.item.quantity > 1) {
                found.item.quantity -= 1;
            } else if (found.item.quantity === 1) {
                $scope.orderList.splice(found.index, 1);
                console.log("item removed");
            }
            foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex].stock += 1;
            $scope.totalItems -= 1;
            $scope.totalBill -= foodMenuService.foodMenu[cuisineType].foodItems[foodItemIndex].price;
        } else {
            console.log("Item not found");
            return false;
        }

        console.log($scope.orderList);
    };

    this.generateOrderId = function() {
        this.orderId += 1;
        return this.orderId;
    }
});

app.service("userAddressService", function() {
    this.address = null;
});

app.filter("getFoodItemByIndex", function() {
    return function(input, foodItemIndex, cuisineType) {
        var i, length = input.length;
        for (i = 0; i < length; i++) {
            if (input[i].foodItemIndex === foodItemIndex && input[i].cuisineType === cuisineType) {
                return {
                    index: i,
                    item: input[i]
                };
            }
        }
        return null;
    }
});