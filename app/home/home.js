"use strict";

angular.module("foodzOnline.home", ["ngRoute"])

.config(["$routeProvider", function($routeProvider) {
    $routeProvider.when("/home", {
        templateUrl: "home/home.html",
        controller: "HomeController"
    });
}])

.controller("HomeController", function($scope, $filter, $location, foodMenuService, orderListService) {
    $scope.isNavCollapsed = true;
    $scope.foodMenu = foodMenuService.foodMenu;
    abc = foodMenuService.foodMenu;
    $scope.orderList = [];
    $scope.totalItems = 0;
    $scope.totalBill = 0;

    $scope.addItem = function(cuisineType, foodItemIndex) {
        orderListService.addItem($scope, $filter, foodMenuService, cuisineType, foodItemIndex);
    }

    $scope.removeItem = function(cuisineType, foodItemIndex) {
        orderListService.removeItem($scope, $filter, foodMenuService, cuisineType, foodItemIndex);
    };

    $scope.toCheckout = function() {
        orderListService.orderList = $scope.orderList;
        orderListService.totalItems = $scope.totalItems;
        orderListService.totalBill = $scope.totalBill;
        $location.path("/"+ "checkout");
    };
});

var abc = null;